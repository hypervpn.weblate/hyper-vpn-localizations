This repository only contains Hyper VPN localizations, currently, sources are closed. https://hypervpn.org

There is three folders:
android/app/src/main/res/values-lang/strings.xml
this is standard Android strings.xml

custom-routing/locale/lang.xml and google-play-description/locale/lang.xml 
these are flat xml files, and result is an html page, so must have html safe strings

also for insert links used this syntax ${link_address}link name${end_link} //insert ${end_link} is mandotary 
for example ${https://www.google.com}Google!${end_link}
